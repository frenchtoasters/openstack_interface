# Openstack Controller

This is a deployment tool for your servers on an openstack instance, it takes a list of servers in your `*.yaml` file and attempts to compare the requested state to the current state and will prefrom the required api actions to get you to the desired state. This is very much a work in progress currently.


## Docker

Use the following commands to build your docker image:

```
cd openstack_interface/
docker build -t openstack_interface .
```

## Python

To install a python binary of this image you simply need to run the following:

```
cd openstack_interface/
python setup.py install
```

You can then execute it by running the following:

```
os-server-config -c os_state.yml
```
