from setuptools import setup, find_packages
import openstack_interface

setup(
    name='openstack_interface',
    version=openstack_interface.__version__,
    author=openstack_interface.__author__,
    description='Python context manager for openstack',
    long_description=open('README.md').read(),
    url='https://gitlab.com/frenchtoasters/openstack_interface',
    keywords=['interface', 'context', 'openstack'],
    license=openstack_interface.__license__,
    packages=find_packages(exclude=['*.test', '*.test.*']),
    include_package_data=True,
    install_requires=open('requirements.txt').readlines(),
    entry_points={
        'console_scripts': [
            'os-server-config=openstack_interface.openstackinterface:main'
        ]
    }
)
