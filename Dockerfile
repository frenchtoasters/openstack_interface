FROM python:3.7

WORKDIR /opt/openstackinterface

# Install Pip requirements
ADD ./requirements.txt /opt/openstackinterface
RUN pip install -r /opt/openstackinterface/requirements.txt

# Installing binary
ADD ./ /opt/openstackinterface
RUN python /opt/openstackinterface/setup.py install

CMD ["python"]
