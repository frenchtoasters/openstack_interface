import argparse
import sys
import os
import yaml
import requests

from keystoneauth1 import loading as ks_loading
from novaclient import client as nova_client
from cinderclient import client as cinder_client

from abc import ABC, abstractmethod

# Disable SSL warnings
requests.packages.urllib3.disable_warnings()


class OpenStack(ABC):

    @abstractmethod
    def _create_server(self, name, image_id, flavor_id, availability_zone, network_id): pass

    @abstractmethod
    def _create_volume(self, size, name, volume_type): pass

    @abstractmethod
    def _create_cgroup(self, name, description, volume_type): pass

    @abstractmethod
    def _create_group_type(self, name, description, is_public): pass

    @abstractmethod
    def _create_group(self, name, description, group_type_id, volume_type, availability_zone): pass

    @abstractmethod
    def _delete_server(self, name): pass

    @abstractmethod
    def _get_server(self, name): pass


class OpenStackClient(OpenStack):
    def __repr__(self):
        return "<Servers: %s>" % self._servers

    def __str__(self):
        return self._servers

    def __init__(self, args):
        self.auth = ks_loading.load_auth_from_argparse_arguments(args)
        self.sess = ks_loading.load_session_from_argparse_arguments(args, auth=self.auth)

        self._nova = nova_client.Client(version=os.getenv('OS_COMPUTE_API_VERSION', '2.1'), session=self.sess)
        self._cinder = cinder_client.Client(version=os.getenv('OS_STORAGE_API_VERSION', '3.14'), session=self.sess)
        self._headers = {}
        self._servers = list()
        self._config = list()

    def _create_server(self, name, image_id, flavor_id, availability_zone, network_id):
        return self._nova.servers.create(name=name, image=image_id, flavor=flavor_id,
                                         availability_zone=availability_zone, nics=[{'net-id': network_id}])

    def _create_cgroup(self, name, description, volume_type):
        return self._cinder.consistencygroups.create(name=name, description=description, volume_types=volume_type)

    def _create_volume(self, size, name, volume_type):
        return self._cinder.volumes.create(size=size, name=name, volume_type=volume_type)

    def _create_group_type(self, name, description, is_public):
        return self._cinder.group_types.create(name=name, description=description, is_public=is_public)

    def _create_group(self, name, description, group_type_id, volume_type, availability_zone):
        return self._cinder.groups.create(group_type=group_type_id, volume_types=volume_type, name=name,
                                          description=description)

    def _delete_server(self, name):
        # Needs to be tested
        return self._nova.servers.force_delete(server=name)

    def _get_server(self, name):
        # Needs testing
        # Could be set as property?
        return self._nova.servers.get(server=name)


class ServerManager(OpenStackClient):
    def __len__(self):
        return len(self._servers)

    def __getitem__(self, position):
        return self._servers[position]

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        # Write state file if there is a server
        if len(self._servers) > 0:
            with open('os_state.yml', 'w') as outfile:
                print("Writing state file")
                yaml.safe_dump(self._servers, outfile)
            outfile.close()

    def create_server(self, name, image, flavor, availability_zone, network, server):
        flavor = self._nova.flavors.find(name=flavor)
        image = self._nova.glance.find_image(image)
        nics = self._nova.neutron.find_network(network)
        server_deploy = self._create_server(name=name, image_id=image.id, flavor_id=flavor.id,
                                            availability_zone=availability_zone, network_id=nics.id)
        print("Server is currently in %s status" % server_deploy.status)
        while server_deploy.status != 'ACTIVE':
            server_deploy = self._nova.servers.get(server_deploy.id)
            if server_deploy.status == 'ERROR':
                print("error deploying server")
                sys.exit(1)

        server['_id'] = server_deploy.id
        server['_name'] = server_deploy.name
        server['_flavor'] = server_deploy.flavor.get('id')
        print("Server is now %s" % server_deploy.status)

        # Find BOOT volume id
        temp_vols = self._nova.volumes.get_server_volumes(server_id=server_deploy.id)
        boot_vol = dict()
        boot_vol['name'] = "BOOT"
        for vol in temp_vols:
            boot_vol['volume_id'] = vol.id

        server.get('_volumes').append(boot_vol)

    def create_cgroup(self, config, server):
        cgroup = self._create_cgroup(name=config.get('cgroup_config').get('name'),
                                     description=config.get('cgroup_config').get('description'),
                                     volume_type=config.get('cgroup_config').get('volume_type'))
        server['cgroup'] = cgroup.id
        # Might want to check the status of the cgroup here as well

    def attach_volumes(self, server_id, volumes, server):
        # Attach volumes to server
        for volume in volumes:
            volume = self._create_volume(size=volume.get('size'), name=volume.get('name'),
                                         volume_type=volume.get('volume_type'))

            attach_status = self._nova.volumes.create_server_volume(server_id=server_id, volume_id=volume.id)

            # Might want to check this later for success or failure
            print(attach_status)
            volume_info = {'volume_id': volume.id, 'name': volume.name}

            print("Volume %s, attached" % volume.name)

            server.get('_volumes').append(volume_info)

    def update_cgroup(self, cgroup_id, volumes, server):
        cgroup = self._cinder.consistencygroups.get(group_id=server.get('_cgroup'))

        for volume in volumes:
            state = self._cinder.volumes.get(volume_id=volume)
            if state.consistencygroup_id == cgroup.id:
                continue
            elif state.consistencygroup_id == '':
                while state.status != 'available' and state.status != 'in-use':
                    print(state.status)
                    state = self._cinder.volumes.get(volume_id=volume)

                cgroup = self._cinder.consistencygroups.get(group_id=cgroup_id)
                while cgroup.status != 'available' and state.status != 'error':
                    print(cgroup.status)
                    cgroup = self._cinder.consistencygroups.get(group_id=cgroup_id)

                print("Volumes %s, attached to group %s" % (volume, cgroup.id))
                server['_cgroup_vols'].append(volume)

        server['_cgroup'] = cgroup.get('id')
        self._cinder.consistencygroups.update(consistencygroup=cgroup.id, add_volumes=server['_cgroup_vols'])
        # Might want to check the status of this once ran?

    # Not implemented
    def resize(self, server_id, flavor_id):
        return self._nova.servers.resize(server=server_id, flavor=flavor_id, disk_config='AUTO')

    def load_servers(self, state, config):
        if state is not None:
            for node in state.get('servers'):
                server = dict(
                    _id=node.get('_id'),
                    _name=node.get('_name'),
                    _flavor=node.get('_flavor'),
                    _image=node.get('_image'),
                    _volumes=node.get('_volumes'),
                    _cgroup=node.get('_cgroup'),
                    _cgroup_vols=node.get('_cgroup_vols'),
                    _availability_zone=node.get('_availability_zone')
                )
                self._servers.append(server)

        if config is not None:
            for node in config.get('servers'):
                server = dict(
                    _id='',
                    _name=node,
                    _image=config.get('servers').get(node).get('image'),
                    _flavor=config.get('servers').get(node).get('flavor'),
                    _network=config.get('servers').get(node).get('network'),
                    _availability_zone=config.get('servers').get(node).get('availability_zone'),
                    _volumes=config.get('servers').get(node).get('volumes'),
                    _cgroup_config=config.get('servers').get(node).get('cgroup_config'),
                    _cgroup_vols=''
                )
                self._config.append(server)

        if state is None and config is not None:
            for node in config.get('servers'):
                server = dict(
                    _id="",
                    _name=config.get('servers').get(node).get('name'),
                    _flavor=config.get('servers').get(node).get('flavor'),
                    _image=config.get('servers').get(node).get('image'),
                    _volumes=list(),
                    _cgroup="",
                    _availability_zone='',
                    _cgroup_vols=list()
                )
                self._servers.append(server)


def main(argv=None):
    parser = argparse.ArgumentParser()
    ks_loading.register_auth_argparse_arguments(parser, sys.argv[1:], default='v3password')
    ks_loading.register_session_argparse_arguments(parser)

    parser.add_argument("-c", "--config", dest="config_file", default=None,
                        help="Configuration file for state of environment")
    args = parser.parse_args()

    with ServerManager(args) as openstack_cloud:

        if args.config_file is not None:
            with open(args.config_file, 'r') as handle:
                config = yaml.safe_load(handle)

            if config.get('servers') is not None:
                servers = config.get('servers')

        else:
            print("Loading defaults for demo")
            config = {}

        try:
            with open('os_state.yml', 'r') as handle:
                state = yaml.safe_load(handle)
            handle.close()
        except Exception as e:
            print("No state file found, build fresh environment: %s" % e)

        if 'state' in locals():
            print("State file found! Reconciling state")
            openstack_cloud.load_servers(state=state, config=config)
            for server in openstack_cloud:
                update_vols = []
                names = []
                volume_ids = []

                if server.get('_volumes') is not None:
                    if len(server.get('_volumes')) < len(config.get('servers')[server.get('_name')].get('volumes')):
                        for name in server.get('_volumes'):
                            names.append(name.get('name'))
                        for volume in config.get('servers')[server.get('_name')].get('volumes'):
                            if volume.get('name') not in names:
                                update_vols.append(volume)

                    openstack_cloud.attach_volumes(server_id=server.get('_id'),
                                                   volumes=update_vols, server=server)

                else:
                    openstack_cloud.attach_volumes(server_id=server.get('_id'),
                                                   volumes=config.get('servers')[server.get('_name')].get('volumes'),
                                                   server=server)

                if state.get('cgroups') is None \
                        and config.get('servers')[server.get('_name')].get('cgroup_config') is not None:
                    print("Create Consistency Group")
                    group = config.get('servers')[server.get('_name')].get('cgroup_config')
                    openstack_cloud.create_cgroup(config=config.get('servers')[server.get('_name')].get('cgroup_config')
                                                  , server=server)

                elif len(update_vols) >= 1:
                    for volume in update_vols:
                        for vol in server.get('_volumes'):
                            if volume.get('name') == vol['name']:
                                volume_ids.append(vol['volume_id'])
                    openstack_cloud.update_cgroup(cgroup_id=state.get('_cgroup'), volumes=volume_ids, server=server)

        else:
            openstack_cloud.load_servers(state=None, config=config)
            for server in openstack_cloud:
                print("Deploying Server")
                openstack_cloud.create_server(name=server['_name'], image=server['_image'],
                                              flavor=server['_flavor'],
                                              availability_zone=server['_availability_zone'],
                                              network=server['_network'], server=server)

                if server.get('volumes'):
                    print("Attaching volumes")
                    openstack_cloud.attach_volumes(server_id=server.get('_id'), volumes=server.get('volumes'),
                                                   server=server)

                if server.get('cgroup_config'):
                    print("Create Consistency Group")
                    openstack_cloud.create_cgroup(config=server.get('cgroup_config'), server=server)
                    volume_ids = []
                    for volume in server.get('_volumes'):
                        volume_ids.append(volume['volume_id'])
                    openstack_cloud.update_cgroup(cgroup_id=server.get('cgroup'), volumes=volume_ids, server=server)


if __name__ == '__main__':
    main()
